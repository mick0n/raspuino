require.config({
	baseUrl: 'js/lib',
	shim: {
		'undescore': { exports: '_'},
		'bootstrap': { deps: ['jquery']},
		'socketio': { exports: 'io'}
	},
	jsx: {
		fileExtension: '.jsx'
	},
	paths: {
		react: 'react',
		JSXTransformer: 'JSXTransformer',
		raspuino: '../raspuino'
	}
});

require([
	'jquery',
	'bootstrap',
	'less',
	'jsx!raspuino/main',
	'jsx!raspuino/faq'
], function($, bootstrap, less, main, faq) {
	window.onhashchange = function router() {
		switch(window.location.hash) {
			case '#sensors':
				return main.render();
			case '#controls':
				return main.render();
			case '#faq':
				return faq.render();
			default:
				main.render();
		}
	};

	main.render()
});