var Arduino = require('duino');

var board;
var ready = false;

module.exports.init = function init() {
	board = new Arduino.Board();

	board.on('ready', function() {
    	ready = true;
    });

    board.on('data', function(mess) {
    	console.log(mess);
    });
};

module.exports.setPin = function setPin(pin, value) {
	if (ready) {
		board.analogWrite(pin, value);
	}
}