define([
	'jquery',
	'underscore'
], function($, _) {
	var socket = new WebSocket('ws://' + window.location.hostname + ':7779');

	var self = this;
	var waitForReadyState = function(callback) {
		if (socket.readyState < 1) {
			setTimeout(function() {
				waitForReadyState(callback);
			}, 500);
		} else {
			callback.apply(self);
		}
	}

	socket.onerror = function() {
		console.log('socket closed unexpectedly');
		socket.close();
	}

	var sendMessage = function(messageData) {
		waitForReadyState(function() {
			socket.send(JSON.stringify(messageData));
		});
	};

	var setMessageHandler = function(messageHandler) {
		socket.onmessage = messageHandler;
	};

	return {
		setMessageHandler: setMessageHandler,
		sendMessage: sendMessage
	};
});
