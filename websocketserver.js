var WebSocketServer = require('ws').Server;
var arduinoRepository = require('./arduinorepository');

var server;

module.exports.init = function init() {
	server = new WebSocketServer({
		port: 7779
	});
	server.on('connection', onConnect);
};

function onConnect(webSocket) {
	console.log('Connection from ' + webSocket.address);
	webSocket.on('message', onMessage);
}

function onMessage(message) {
	var object = JSON.parse(message);
	if (object.pin !== undefined && object.value !== undefined) {
		arduinoRepository.setPin(object.pin, object.value);
	}
}