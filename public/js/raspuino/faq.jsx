define([
	'jquery',
	'react',
	'react-dom',
	'raspuino/socketconnection',
	'jsx!raspuino/led'
], function($, React, ReactDOM, socket, Led) {
	var Main = React.createClass({
		getInitialState: function() {
			return {
				faq: [
					{
						question: 'Question A',
						answer: 'Answer A'
					},
					{
						question: 'Question B',
						answer: 'Answer B'
					}
				]
			};
		},
		render: function() {
			return (
				<div className='row'>

				</div>
			);
		}
	});

	function render() {
		ReactDOM.render(<Main />
		, document.getElementById('root'));
	}

	return {
		render: render
	}
});