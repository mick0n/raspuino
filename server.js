var express = require('express');
var arduinoRepository = require('./arduinorepository');
var webSocketServer = require('./websocketserver');

var systemSetup = require('jsonfile').readFileSync('system.json', {encoding: 'UTF-8'});

var app = new express();
var emulate = false;
if (process.argv.length > 2 && process.argv[2] === '--emulate') {
	emulate = true;
}

app.use(express.static('public'));
app.use(require('body-parser').urlencoded({extended: false}));

app.get('/system/list', function(req, res) {
	res.send(systemSetup);
});

app.listen(7777, function() {
	console.log('Server started on port 7777');
	if(!emulate) {
		arduinoRepository.init();
	}
	webSocketServer.init();
});
