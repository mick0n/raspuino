define([
	'jquery',
	'react',
	'react-dom',
	'nouislider',
	'raspuino/socketconnection'
], function($, React, ReactDOM, noUISlider, socket) {
	var slider;

	return React.createClass({
		getInitialState: function() {
			return {
				value: 0
			}
		},
		componentDidMount: function() {
			slider = this.refs.slider;
			noUISlider.create(slider, {
				start: [0],
				connect: 'lower',
				step: 1,
				range: {
					'min': 0,
					'max': 255
				}
			});
			var component = this;
			slider.noUiSlider.on('slide', function(value, handle, number) {
				var parsedValue = parseInt(value);
				component.setState({value: parsedValue});
				socket.sendMessage({
					pin: component.props.pin,
					value: parsedValue
				});
			});
		},
		handleSlide: function() {

		},
		render: function() {
			return (
				<div className='component-led panel panel-default'>
					<div className='panel-heading'>
						<h3 className='panel-title'>Pin <span title='Pin number' className='squared-number'>{this.props.pin}</span> - <span className='gray'>{this.props.name}</span></h3>
					</div>
					<div className='panel-body'>
						<div className='left-column'>
							<div>{this.state.value}</div>
						</div>
						<div className='right-column'>
							<div className='slider' ref='slider'></div>
						</div>
					</div>
				</div>
			);
		}
	});
});