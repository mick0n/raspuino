var duino = require('duino');

var board = new duino.Board();

var led = new duino.Led({
        board: board,
	pin: 5
});

var loop = function() {
	board.analogWrite(5, Math.random() * 255);
	board.analogWrite(3, Math.random() * 128);
	setTimeout(loop, 500);	
}

board.on('ready', function() {
/*	var led = new duino.Led({
        	board: board,
	        pin: 5
	});


	led.blink();*/
	led.on();
	loop();
});

board.on('data', function(mess) {
	console.log(mess);
});
