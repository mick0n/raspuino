define([
	'jquery',
	'react',
	'react-dom',
	'raspuino/socketconnection',
	'jsx!raspuino/led'
], function($, React, ReactDOM, socket, Led) {
	var Main = React.createClass({
		getInitialState: function() {
			return {
				pinConfigs: []
			};
		},
		componentDidMount: function() {
			var component = this;
			$.get('/system/list', function(data) {
				data.pins.forEach(function(pinData) {
					var pinConfigs = component.state.pinConfigs;
					pinConfigs.push(pinData);
					component.setState({pinConfigs: pinConfigs});
				});
			});
		},
		render: function() {
			return (
				<div>
					{this.state.pinConfigs.map(function(pinConfig) {
						if (pinConfig.type === 'led') {
							return <Led pin={pinConfig.id} name={pinConfig.name} />
						}
						return <div>Unknown component - {pinConfig.name} ({pinConfig.type})</div>
					})}
				</div>
			);
		}
	});

	function render() {
		ReactDOM.render(<Main />
		, document.getElementById('root'));
	}

	return {
		render: render
	}
});